#ifndef __glext_mgltools_h_
#define __glext_mgltools_h_

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_WIN32) && !defined(APIENTRY) && !defined(__CYGWIN__) && !defined(__SCITECH_SNAP__)
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#endif

#ifndef APIENTRY
#define APIENTRY
#endif
#ifndef APIENTRYP
#define APIENTRYP APIENTRY *
#endif
#ifndef extern
#define extern extern
#endif

/********************************* gl.h definitions *******************/
typedef unsigned int GLenum;
typedef unsigned char GLboolean;
typedef unsigned int GLbitfield;
typedef signed char GLbyte;
typedef short GLshort;
typedef int GLint;
typedef int GLsizei;
typedef unsigned char GLubyte;
typedef unsigned short GLushort;
typedef unsigned int GLuint;
typedef float GLfloat;
typedef float GLclampf;
typedef double GLdouble;
typedef double GLclampd;
typedef void GLvoid;

/**********************************************************************/

/********************************* glext.h definitions *******************/
#include <stddef.h>
#ifndef GL_VERSION_2_0
/* GL type for program/shader text */
typedef char GLchar;			/* native character */
#endif

#ifndef GL_VERSION_1_5
/* GL types for handling large vertex buffer objects */
typedef ptrdiff_t GLintptr;
typedef ptrdiff_t GLsizeiptr;
#endif

#ifndef GL_ARB_vertex_buffer_object
/* GL types for handling large vertex buffer objects */
typedef ptrdiff_t GLintptrARB;
typedef ptrdiff_t GLsizeiptrARB;
#endif

#ifndef GL_ARB_shader_objects
/* GL types for handling shader object handles and program/shader text */
typedef char GLcharARB;		/* native character */
typedef unsigned int GLhandleARB;	/* shader object handle */
#endif

/* GL types for "half" precision (s10e5) float data in host memory */
#ifndef GL_ARB_half_float_pixel
typedef unsigned short GLhalfARB;
#endif

#ifndef GL_NV_half_float
typedef unsigned short GLhalfNV;
#endif

#ifndef GL_EXT_timer_query
typedef signed long long GLint64EXT;
typedef unsigned long long GLuint64EXT;
#endif
/**********************************************************************/


/************ add here the needed functionalities from glext.h **********/

/****************** shaders *********************************************/
#define GL_COMPILE_STATUS                 0x8B81
#define GL_FRAGMENT_SHADER                0x8B30
#define GL_LINK_STATUS                    0x8B82
#define GL_VALIDATE_STATUS                0x8B83
#define GL_VERTEX_SHADER                  0x8B31

extern GLenum APIENTRY glCheckFramebufferStatusEXT(GLenum);
extern void APIENTRY glActiveTexture(GLenum);
extern GLuint APIENTRY glCreateShader (GLenum);
extern void APIENTRY glAttachShader (GLuint, GLuint);
extern void APIENTRY glCompileShader (GLuint);
extern GLuint APIENTRY glCreateProgram (void);
extern void APIENTRY glGetProgramiv (GLuint, GLenum, GLint * aInt);
extern void APIENTRY glGetProgramInfoLog (GLuint, GLsizei, GLsizei *, GLchar *);
extern void APIENTRY glGetShaderiv (GLuint, GLenum, GLint * aInt);
extern void APIENTRY glGetShaderInfoLog (GLuint, GLsizei, GLsizei *, GLchar *);
extern GLint APIENTRY glGetUniformLocation (GLuint, const GLchar *);
extern void APIENTRY glLinkProgram (GLuint);
extern void APIENTRY glShaderSource (GLuint, GLsizei, const GLchar * *, const GLint *);
extern void APIENTRY glUniform1i (GLint, GLint);
extern void APIENTRY glUniform1f (GLint, GLfloat);
extern void APIENTRY glUniform4f (GLint, GLfloat, GLfloat, GLfloat, GLfloat);
extern void APIENTRY glUseProgram (GLuint);
extern void APIENTRY glValidateProgram (GLuint);
/**********************************************************************/

/****************** frame buffer object *******************************/
#define GL_COLOR_ATTACHMENT0_EXT		  0x8CE0
#define GL_DEPTH_ATTACHMENT_EXT           0x8D00
#define GL_DEPTH_STENCIL_EXT              0x84F9
#define GL_FRAMEBUFFER_EXT				  0x8D40
#define GL_STENCIL_ATTACHMENT_EXT         0x8D20
#define GL_FRAMEBUFFER_COMPLETE_EXT       0x8CD5

extern void APIENTRY glBindFramebufferEXT (GLenum, GLuint);
extern GLenum APIENTRY glCheckFramebufferStatusEXT (GLenum);
extern void APIENTRY glFramebufferTexture2DEXT (GLenum, GLenum, GLenum, GLuint, GLint);
extern void APIENTRY glGenFramebuffersEXT (GLsizei, GLuint *);

// duplicated gl calls
// this one allows to pass None for a NULL pointer
//extern void APIENTRY glTexImage2D (GLenum target, GLint level, GLint internalformat,
//		                           GLsizei width, GLsizei height, GLint border,
//		                           GLenum format, GLenum type, const GLvoid * pixels);
/**********************************************************************/

/****************** vertex buffer object *******************************/
#define GL_ARRAY_BUFFER_ARB               0x8892
#define GL_ELEMENT_ARRAY_BUFFER           0x8893
#define GL_STATIC_DRAW_ARB                0x88E4
#define GL_DYNAMIC_DRAW_ARB               0x88E8
#define GL_STREAM_DRAW_ARB                0x88E0

/****************** defines needed for SSOA *****************************/
#define GL_DEPTH_TEXTURE_MODE             0x884B
#define GL_TEXTURE_COMPARE_MODE           0x884C
#define GL_DEPTH_COMPONENT32              0x81A7
#define GL_CLAMP_TO_BORDER                0x812D
#define GL_TEXTURE0                       0x84C0
#define GL_TEXTURE1                       0x84C1
#define GL_TEXTURE2                       0x84C2
#define GL_TEXTURE3                       0x84C3
#define GL_TEXTURE4                       0x84C4
#define GL_TEXTURE5                       0x84C5
#define GL_TEXTURE6                       0x84C6
#define GL_TEXTURE7                       0x84C7
#define GL_TEXTURE8                       0x84C8
#define GL_TEXTURE9                       0x84C9
#define GL_TEXTURE10                      0x84CA


extern void APIENTRY glGenBuffersARB (GLsizei, GLuint * buffers);
extern void APIENTRY glBindBufferARB (GLenum, GLuint);
extern void APIENTRY glBufferDataARB (GLenum, GLsizeiptrARB, const GLvoid *, GLenum);
extern void APIENTRY glDeleteBuffersARB (GLsizei, const GLuint *);

//extern GLvoid* APIENTRY glMapBufferARB (GLenum, GLenum);
//extern GLboolean APIENTRY glUnmapBufferARB (GLenum);

// duplicated gl calls
// this one allows to pass an integer as an offset relative to the VBO pointer 
//extern void APIENTRY glVertexPointer (GLint size, GLenum type, 
//		                              GLsizei stride, const GLvoid * pointer);
//extern void APIENTRY glNormalPointer (GLenum type, GLsizei stride,
//		                              const GLvoid *pointer);
//extern void APIENTRY glDrawElements (GLenum mode, GLsizei count, GLenum type,
//		                             const GLvoid *indices);

/**********************************************************************/



/**********************************************************************/


#ifdef __cplusplus
}
#endif

#endif /* __glext_mgltools_h_ */

