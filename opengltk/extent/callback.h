/* automatically generated */

#ifndef _MSC_VER
#   undef WSTDCALL
#   undef WVARCALL
#   define WSTDCALL
#   define WVARCALL
#endif
typedef void (WSTDCALL *void_void_f)(void);

typedef void (WSTDCALL *void_GLenum_f)( GLenum which);

typedef void (WSTDCALL *void_int_f)( int arg0);

typedef void (WSTDCALL *void_int_int_f)( int arg0, int arg1);

typedef void (WSTDCALL *void_int_int_int_f)( int arg0, int arg1, int arg2);

typedef void (WSTDCALL *void_int_int_int_int_f)( int arg0, int arg1, int arg2, int arg3);

typedef void (WSTDCALL *void_unsignedchar_int_int_f)( unsigned char arg0, int arg1, int arg2);

typedef void (WSTDCALL *void_unsignedint_int_int_int_f)( unsigned int arg0, int arg1, int arg2, int arg3);

typedef void (WSTDCALL *void_int_voidstar_f)( int view, void* context);

